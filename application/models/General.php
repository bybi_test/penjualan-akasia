<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Model {

    public function inserdata($table, $data){
        $this->db->insert($table, $data);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function updateData($table, $id, $data){
        $this->db->where('id', $id);
        $ret = $this->db->update($table, $data);
        return $ret;
    }


    public function deleteData($table, $where) {
        $this->db->where($where);
        $ret = $this->db->delete($table);
        return $ret;
    }

    public function getData($table, $orderKey, $orderValue){
        $this->db->order_by($orderKey, $orderValue);
        $ret = $this->db->get($table)->result();
        return $ret;
    }

    public function getDataWhere($table, $where) {
        $ret = $this->db->get_where($table, $where)->row();
        return $ret;
    }

    public function getProduct() {
        $this->db->select('product.*, supplier.name as supplier_name');
        $this->db->from('product');
        $this->db->join('supplier', 'supplier.id = product.supplier_id');
        $this->db->order_by('product.id', 'DESC');
        $ret = $this->db->get()->result();
        return $ret;
    }

}