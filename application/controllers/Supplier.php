<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('General');
    }

    // index of supplier
    public function index() {
        $this->data['title']      = "supplier";
        $this->data['subview']    = "supplier/index";
        $this->data['data'] = $this->General->getData('supplier', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // create of supplier
    public function add() {
        $this->data['title']      = "supplier";
        $this->data['subview']    = "supplier/create";
        $this->load->view('layouts/main', $this->data);
    }

    // store of supplier
    public function store() {
        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']      = "supplier";
            $this->data['subview']    = "supplier/create";
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'		=> $this->input->post('name'),
			];
			$this->General->inserdata('supplier', $data);
            redirect('/supplier');
        }
    }

    // edit of supplier
    public function edit($id) {
        $this->data['title']    = "supplier";
        $this->data['subview']  = "supplier/edit";
        $this->data['data']     = $this->General->getDataWhere('supplier', ['id' => $id]);
        $this->load->view('layouts/main', $this->data);
    }

    // update of supplier
    public function update($id) {
        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']      = "supplier";
            $this->data['subview']    = "supplier/create";
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'		=> $this->input->post('name'),
			];
			$this->General->updateData('supplier', $id, $data);
            redirect('/supplier');
        }
    }

    // delete of supplier
    public function delete($id) {
        $this->General->deleteData('supplier', ['id' => $id]);
		redirect('/supplier');
    }
}
