<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('General');
    }

    // index of transaction
    public function index() {
        $this->data['title']      = "transaction";
        $this->data['subview']    = "transaction/index";
        $this->data['data'] = $this->General->getData('transaction', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // sale of transaction
    public function sale() {
        $this->data['title']      = "sale";
        $this->data['subview']    = "transaction/sale";
        $this->data['data'] = $this->General->getData('product', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // save of sale
    public function saveData() {
        $inputPayment = $this->input->post('payment');

        $payment = [
            'user_id'       => 6,
            'created_at'    => date("Y-m-d h:i:s"),
            'total_price'   => $inputPayment['total_price'],
            'total_money'   => $inputPayment['total_money'],
            'change_money'  => $inputPayment['change_money'],
        ];

        $savePayment = $this->General->inserdata('transaction', $payment);

        $inputItems = $this->input->post('items');
        foreach ($inputItems as $key => $value) {
            $items = [
                'transaction_id'    => $savePayment,
                'product_id'        => $inputItems[$key]['product_id'],
                'supplier_id'       => $inputItems[$key]['supplier_id'],
                'quantity'          => $inputItems[$key]['quantity'],
                'total'             => $inputItems[$key]['total'],
            ];
            $saveItems = $this->General->inserdata('detail_transaction', $items);
        }
        echo json_encode($inputItems);
    }
}