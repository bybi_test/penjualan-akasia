<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('General');
    }

    // index of users
    public function index() {
        $this->data['title']      = "users";
        $this->data['subview']    = "users/index";
        $this->data['data'] = $this->General->getData('users', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // create of users
    public function add() {
        $this->data['title']      = "users";
        $this->data['subview']    = "users/create";
        $this->load->view('layouts/main', $this->data);
    }

    // store of users
    public function store() {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']      = "users";
            $this->data['subview']    = "users/create";
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'		=> $this->input->post('name'),
				'email' 	=> $this->input->post('email'),
				'password'  => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			];
			$this->General->inserdata('users', $data);
            redirect('/users');
        }
    }

    // edit of users
    public function edit($id) {
        $this->data['title']    = "users";
        $this->data['subview']  = "users/edit";
        $this->data['data']     = $this->General->getDataWhere('users', ['id' => $id]);
        $this->load->view('layouts/main', $this->data);
    }

    // update of users
    public function update($id) {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']      = "users";
            $this->data['subview']    = "users/create";
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'		=> $this->input->post('name'),
				'email' 	=> $this->input->post('email'),
			];
            if ($this->input->post('password')) {
                $data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }
			$this->General->updateData('users', $id, $data);
            redirect('/users');
        }
    }

    // delete of users
    public function delete($id) {
        $this->General->deleteData('users', ['id' => $id]);
		redirect('/users');
    }
}
