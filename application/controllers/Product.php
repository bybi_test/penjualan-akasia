<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('General');
    }

    // index of product
    public function index() {
        $this->data['title']    = "product";
        $this->data['subview']  = "product/index";
        $this->data['data']     = $this->General->getProduct('product', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // create of product
    public function add() {
        $this->data['title']    = "product";
        $this->data['subview']  = "product/create";
        $this->data['supplier'] = $this->General->getData('supplier', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // store of product
    public function store() {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required');
        $this->form_validation->set_rules('supplier', 'Supplier', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']    = "product";
            $this->data['subview']  = "product/create";
            $this->data['supplier'] = $this->General->getData('supplier', 'id', 'desc');
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'          => $this->input->post('name'),
                'price'         => $this->input->post('price'),
                'stock'         => $this->input->post('stock'),
                'supplier_id'   => $this->input->post('supplier'),
                'user_id'       => 6,
			];
			$this->General->inserdata('product', $data);
            redirect('/product');
        }
    }

    // edit of product
    public function edit($id) {
        $this->data['title']    = "product";
        $this->data['subview']  = "product/edit";
        $this->data['data']     = $this->General->getDataWhere('product', ['id' => $id]);
        $this->data['supplier'] = $this->General->getData('supplier', 'id', 'desc');
        $this->load->view('layouts/main', $this->data);
    }

    // update of product
    public function update($id) {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required');
        $this->form_validation->set_rules('supplier', 'Supplier', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->data['title']      = "product";
            $this->data['subview']    = "product/create";
            $this->data['data']     = $this->General->getDataWhere('product', ['id' => $id]);
            $this->data['supplier'] = $this->General->getData('supplier', 'id', 'desc');
            $this->load->view('layouts/main', $this->data);
        } else {
            $data = [
                'name'          => $this->input->post('name'),
                'price'         => $this->input->post('price'),
                'stock'         => $this->input->post('stock'),
                'supplier_id'   => $this->input->post('supplier'),
                'user_id'       => 6,
			];
			$this->General->updateData('product', $id, $data);
            redirect('/product');
        }
    }

    // delete of product
    public function delete($id) {
        $this->General->deleteData('product', ['id' => $id]);
		redirect('/product');
    }
}
