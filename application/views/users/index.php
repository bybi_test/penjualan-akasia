<div class="card">

  <div class="card-header d-flex justify-content-between">
    <h4><?= $title ?></h4>
    <a href="/users/add" class="btn btn-primary">ADD</a>
  </div>

  <div class="card-body">    
    <table id="datatable" class="display">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th width="175" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $value) { ?>
                <tr>
                    <td><?= $value->name ?></td>
                    <td><?= $value->email ?></td>
                    <td class="text-center">
                        <a href="/users/edit/<?= $value->id ?>" class="btn btn-primary m-r-5">Edit</a>
                        <a href="/users/delete/<?= $value->id ?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
  </div>

</div>