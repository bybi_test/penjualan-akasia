<div class="card">

  <div class="card-header">
    <h4><?= $title ?></h4>
  </div>

  <div class="card-body">
    <?php echo validation_errors(); ?>
    <?= form_open('/users/store'); ?>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?= set_value('name') ?>">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="<?= set_value('email') ?>">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>

</div>