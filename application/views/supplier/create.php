<div class="card">

  <div class="card-header">
    <h4><?= $title ?></h4>
  </div>

  <div class="card-body">
    <?php echo validation_errors(); ?>
    <?= form_open('/supplier/store'); ?>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?= set_value('name') ?>">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>

</div>