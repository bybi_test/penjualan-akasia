<div class="card">

  <div class="card-header">
    <h4><?= $title ?></h4>
  </div>

  <div class="card-body">
    <?php echo validation_errors(); ?>
    <?= form_open('/product/store'); ?>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?= set_value('name') ?>">
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="price">Price</label>
            <input type="number" class="form-control" id="price" name="price" value="<?= set_value('price') ?>">
          </div>
          <div class="form-group col-md-4">
            <label for="stock">Stock</label>
            <input type="number" class="form-control" id="stock" name="stock" value="<?= set_value('stock') ?>">
          </div>
          <div class="form-group col-md-4">
            <label for="supplier">Supplier</label>
            <select id="supplier" name="supplier" class="form-control">
              <option selected disabled>Choose</option>
              <?php foreach ($supplier as $value) { ?>
                <option value="<?= $value->id ?>" <?= set_value('supplier') == $value->id ? 'selected' : '' ?> ><?= $value->name ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>

</div>