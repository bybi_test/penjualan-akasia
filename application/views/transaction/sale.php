<div class="card">

    <div class="card-header d-flex justify-content-between">
        <h4><?= $title ?></h4>
    </div>

    <div class="card-body">
        <h5 class="mb-4">Product</h5>
        <table id="datatable" class="table table-striped">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th width="75" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $key => $value) { ?>
                    <tr id="row-<?= $key+1 ?>" data-id="<?= $value->id ?>" data-supplier="<?= $value->supplier_id ?>">
                        <th scope="row"><?= $key+1 ?></th>
                        <td class="name"><?= $value->name ?></td>
                        <td class="price"><?= $value->price ?></td>
                        <td class="stock"><?= $value->stock ?></td>
                        <td class="text-center">
                            <button onclick="add(<?= $key+1 ?>)" class="btn btn-primary">+</button>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <h5 class="my-4">Payment</h5>
        <table id="table-items" class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th width="100" class="text-center">Quantity</th>
                    <th width="150" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

        <table id="table-payment" class="table">
            <tr>
                <td class="text-right">Total Price</td>
                <td width="150" class="text-right total-price"></td>
                <td width="150"></td>
            </tr>
            <tr>
                <td class="text-right">Total Money</td>
                <td width="150" class="text-right">
                    <input type="number" class="form-control total-money">
                </td>
                <td width="150"></td>
            </tr>
            <tr>
                <td class="text-right">Change Money</td>
                <td width="150" class="text-right change-money"></td>
                <td width="150"></td>
            </tr>
        </table>
    </div>

    <div class="d-flex justify-content-end m-3">
        <button class="btn btn-primary" onclick="save()">save</button>
    </div>
</div>

<script>
    var paymentId = [];
    var items = {};
    var totalPrice = 0;

    function add(row) {
        idRow = "#row-"+row;

        let id = $(idRow).data('id');
        let supplierId = $(idRow).data('supplier');
        let name = $(`${idRow} .name`).html();
        let price = parseInt($(`${idRow} .price`).html());
        let stock = parseInt($(`${idRow} .stock`).html());

        if (paymentId.includes(row)) {
            plus(row, price);
        } else {
            $("#table-items tbody").append(`
                <tr id="row-${row}">
                    <th>${name}</th>
                    <th class="price">${price}</th>
                    <td class="quantity">
                        <input type="number" class="form-control text-center" value="1">
                    </td>
                    <td class="text-center">
                        <button onclick="plus(${row},${price})" class="btn btn-primary m-r-5">+</button>
                        <button onclick="minus(${row},${price})" class="btn btn-danger">-</button>
                    </td>
                </tr>
            `);
            paymentId.push(row);
            items[row] = {
                'product_id': id,
                'supplier_id': supplierId,
                'quantity': 1,
                'price': price,
                'total': price
            };
            handlePayment(price, 'plus');
        }
    }

    function plus(row, price) {
        idRow = "#table-items #row-"+row;
        qty = parseInt($(idRow+" .quantity input").val());
        qtyNew = qty+1;

        $(idRow+" .price").html(price*qtyNew);
        $(idRow+" .quantity input").val(qtyNew);
        
        items[row].quantity = qtyNew;
        items[row].total = price*qtyNew;
        handlePayment(price, 'plus');
    }


    function minus(row, price) {
        idRow = "#table-items #row-"+row;
        qty = parseInt($(idRow+" .quantity input").val());
        qtyNew = qty-1;

        if (qtyNew > 0) {
            $(idRow+" .price").html(price*qtyNew);
            $(idRow+" .quantity input").val(qtyNew);
        
            items[row].quantity = qtyNew;
            items[row].total = price*qtyNew;
            handlePayment(price, 'minus');
        } else {
            const index = paymentId.indexOf(row);
            const x = paymentId.splice(index, 1);
            $(idRow).remove();
            delete items[row];
            handlePayment(price, 'minus');
        }
    }

    function handlePayment(price, status) {
        totalPrice = status == 'plus' ? totalPrice+price : totalPrice-price;

        $("#table-payment .total-price").html(totalPrice);
        handleChangeMoney();
    }

    function handleChangeMoney() {
        totalMoney = $(".total-money").val();
        if (totalMoney) {
            changeMoney = totalMoney-totalPrice;
            
            $("#table-payment .change-money").html(changeMoney);
        } else {
            $("#table-payment .change-money").html();
        }
    }

    function save() {
        totalMoney = $(".total-money").val();
        changeMoney = parseInt($(".change-money").html());

        payment = {
            'total_price': totalPrice,
            'total_money': totalMoney,
            'change_money': changeMoney
        };
        $.ajax({
            type : 'POST',
            data : {
                payment:payment,
                items:items,
            },
            url  : '<?= base_url('/transaction/saveData') ?>',
            dataType : 'json',
            success : function(res) {
                location.reload();
            }
        });
    }

    $(document).ready(function(){
        $(".total-money").keyup(function(){
            totalMoney = $(this).val();
            handleChangeMoney();
        });
    });
</script>