<div class="card">

  <div class="card-header d-flex justify-content-between">
    <h4><?= $title ?></h4>
    <a href="/users/add" class="btn btn-primary">ADD</a>
  </div>

  <div class="card-body">    
    <table id="datatable" class="display">
        <thead>
            <tr>
                <th>Total Price</th>
                <th>Total Money</th>
                <th>Change Money</th>
                <!-- <th width="175" class="text-center">Action</th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $value) { ?>
                <tr>
                    <td><?= $value->total_price ?></td>
                    <td><?= $value->total_money ?></td>
                    <td><?= $value->change_money ?></td>
                    <!-- <td class="text-center">
                        <a href="/transaction/edit/<?= $value->id ?>" class="btn btn-primary m-r-5">Edit</a>
                        <a href="/transaction/delete/<?= $value->id ?>" class="btn btn-danger">Delete</a>
                    </td> -->
                </tr>
            <?php } ?>
        </tbody>
    </table>
  </div>

</div>